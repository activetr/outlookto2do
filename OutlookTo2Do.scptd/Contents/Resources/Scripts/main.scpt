FasdUAS 1.101.10   ��   ��    k             l     ��  ��    ; 5-----------------------------------------------------     � 	 	 j - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -   
  
 l     ��  ��    < 6--------         Outlook To 2Do             ----------     �   l - - - - - - - -                   O u t l o o k   T o   2 D o                           - - - - - - - - - -      l     ��  ��    ; 5-----------------------------------------------------     �   j - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -      l     ��  ��    1 + Author: James Gibbard (jgibbard@gmail.com)     �   V   A u t h o r :   J a m e s   G i b b a r d   ( j g i b b a r d @ g m a i l . c o m )      l     ��  ��      Version: 1.00     �      V e r s i o n :   1 . 0 0      l     ��������  ��  ��       !   l     �� " #��   "   Last Updated: 06/02/2014    # � $ $ 2   L a s t   U p d a t e d :   0 6 / 0 2 / 2 0 1 4 !  % & % l     ��������  ��  ��   &  ' ( ' l     �� ) *��   )   Description:    * � + +    D e s c r i p t i o n : (  , - , l     �� . /��   . 3 - An AppleScript to create tasks (in 2Do) from    / � 0 0 Z   A n   A p p l e S c r i p t   t o   c r e a t e   t a s k s   ( i n   2 D o )   f r o m -  1 2 1 l     �� 3 4��   3 4 . Outlook messages. It can be used as a script     4 � 5 5 \   O u t l o o k   m e s s a g e s .   I t   c a n   b e   u s e d   a s   a   s c r i p t   2  6 7 6 l     �� 8 9��   8 "  called from a shortcut key.    9 � : : 8   c a l l e d   f r o m   a   s h o r t c u t   k e y . 7  ; < ; l     ��������  ��  ��   <  = > = l     �� ? @��   ?   Dependent Apps:    @ � A A     D e p e n d e n t   A p p s : >  B C B l     �� D E��   D 0 * - Outlook for Mac (http://microsoft.com/)    E � F F T   -   O u t l o o k   f o r   M a c   ( h t t p : / / m i c r o s o f t . c o m / ) C  G H G l     �� I J��   I ) # - 2Do (http://www.2doapp.com/mac/)    J � K K F   -   2 D o   ( h t t p : / / w w w . 2 d o a p p . c o m / m a c / ) H  L M L l     ��������  ��  ��   M  N O N l     �� P Q��   P ; 5-----------------------------------------------------    Q � R R j - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - O  S T S l     �� U V��   U 8 2           PROPERTIES TO BE AJUSTED             --    V � W W d                       P R O P E R T I E S   T O   B E   A J U S T E D                           - - T  X Y X l     �� Z [��   Z ; 5-----------------------------------------------------    [ � \ \ j - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - Y  ] ^ ] l     ��������  ��  ��   ^  _ ` _ l     �� a b��   a   Property: defaultTag    b � c c *   P r o p e r t y :   d e f a u l t T a g `  d e d l     �� f g��   f / ) Sets the default tag that is assinged to    g � h h R   S e t s   t h e   d e f a u l t   t a g   t h a t   i s   a s s i n g e d   t o e  i j i l     �� k l��   k - ' every task that is created from a note    l � m m N   e v e r y   t a s k   t h a t   i s   c r e a t e d   f r o m   a   n o t e j  n o n j     �� p�� 0 
defaulttag 
defaultTag p m      q q � r r 
 E m a i l o  s t s l     ��������  ��  ��   t  u v u l     �� w x��   w   Property: userTags    x � y y &   P r o p e r t y :   u s e r T a g s v  z { z l     �� | }��   | 4 . Allows the ability to specify if user defined    } � ~ ~ \   A l l o w s   t h e   a b i l i t y   t o   s p e c i f y   i f   u s e r   d e f i n e d {   �  l     �� � ���   � . ( tags should be turned ON (1) or OFF (0)    � � � � P   t a g s   s h o u l d   b e   t u r n e d   O N   ( 1 )   o r   O F F   ( 0 ) �  � � � j    �� ��� 0 usertags userTags � m    ����  �  � � � l     ��������  ��  ��   �  � � � l     �� � ���   �   Property: tagList    � � � � $   P r o p e r t y :   t a g L i s t �  � � � l     �� � ���   � 1 + Sets out the userTags that can be selected    � � � � V   S e t s   o u t   t h e   u s e r T a g s   t h a t   c a n   b e   s e l e c t e d �  � � � l     �� � ���   � ( " when creating a task from a note     � � � � D   w h e n   c r e a t i n g   a   t a s k   f r o m   a   n o t e   �  � � � j    �� ��� 0 taglist tagList � J     � �  � � � m     � � � � � 
 R e p l y �  � � � m     � � � � �  R e a d �  � � � m    	 � � � � �  R e v i e w �  � � � m   	 
 � � � � �  A c t i o n �  ��� � m   
  � � � � �  C r e a t e   T a s k s��   �  � � � l     ��������  ��  ��   �  � � � j    �� ��� 0 categoryname categoryName � m     � � � � �  T a s k   C r e a t e d �  � � � l     ��������  ��  ��   �  � � � l     �� � ���   � ; 5-----------------------------------------------------    � � � � j - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - �  � � � l     �� � ���   � 0 *  MAIN SCRIPT (EXECUTION STARTS HERE)   --    � � � � T     M A I N   S C R I P T   ( E X E C U T I O N   S T A R T S   H E R E )       - - �  � � � l     �� � ���   � ; 5-----------------------------------------------------    � � � � j - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - �  � � � l     ��������  ��  ��   �  � � � l   � ����� � O    � � � � k   � � �  � � � l   ��������  ��  ��   �  � � � l   �� � ���   � 1 + Make sure we only have one mesage selected    � � � � V   M a k e   s u r e   w e   o n l y   h a v e   o n e   m e s a g e   s e l e c t e d �  � � � r     � � � I   �� ���
�� .corecnte****       **** � 1    ��
�� 
CMgs��   � o      ���� 0 msgcount msgCount �  � � � Z      � ����� � l    ����� � A     � � � o    ���� 0 msgcount msgCount � m    ���� ��  ��   � k     � �  � � � l   �� � ���   � D > Throw error and exit, if we can't select the note in Evernote    � � � � |   T h r o w   e r r o r   a n d   e x i t ,   i f   w e   c a n ' t   s e l e c t   t h e   n o t e   i n   E v e r n o t e �  � � � I   �� ���
�� .sysodlogaskr        TEXT � m     � � � � � ^ E R R O R :   P l e a s e   o n l y   s e l e c t   o n e   m e s s a g e   a t   a   t i m e��   �  ��� � L    ����  ��  ��  ��   �  � � � l  ! !��������  ��  ��   �  � � � l  ! !�� � ���   � + % Assign the list of selected messages    � � � � J   A s s i g n   t h e   l i s t   o f   s e l e c t e d   m e s s a g e s �  � � � r   ! & � � � 1   ! $��
�� 
CMgs � o      ���� $0 selectedmessages selectedMessages �  � � � l  ' '��������  ��  ��   �  � � � l  ' '�� � ���   � , & Looping through the selected messages    � � � � L   L o o p i n g   t h r o u g h   t h e   s e l e c t e d   m e s s a g e s �  ��  X   '��� k   7�  l  7 7��������  ��  ��    l  7 7��	��   R L extract the subject, sender name, message ID and the content of the message   	 �

 �   e x t r a c t   t h e   s u b j e c t ,   s e n d e r   n a m e ,   m e s s a g e   I D   a n d   t h e   c o n t e n t   o f   t h e   m e s s a g e  l  7 7����   4 .  and assign it to the corresponding variables    � \     a n d   a s s i g n   i t   t o   t h e   c o r r e s p o n d i n g   v a r i a b l e s  r   7 < n   7 : 1   8 :��
�� 
subj o   7 8���� 0 	mymessage 	myMessage o      ���� 0 	mysubject 	mySubject  r   = B n   = @ 1   > @��
�� 
sndr o   = >���� 0 	mymessage 	myMessage o      ���� 0 mysender mySender  r   C H n   C F !  1   D F��
�� 
pMHd! o   C D���� 0 	mymessage 	myMessage o      ���� 0 myheader myHeader "#" r   I U$%$ 4   I Q��&
�� 
cCtg& o   K P���� 0 categoryname categoryName% o      ���� 0 
mycategory 
myCategory# '(' r   V ])*) m   V Y++ �,,  * o      ���� 0 thecustomtags theCustomTags( -.- l  ^ ^��������  ��  ��  . /0/ l  ^ ^��12��  1 , & Apply the tags to include on the task   2 �33 L   A p p l y   t h e   t a g s   t o   i n c l u d e   o n   t h e   t a s k0 454 r   ^ w676 l  ^ s8����8 I  ^ s��9:
�� .gtqpchltns    @   @ ns  9 o   ^ c���� 0 taglist tagList: ��;<
�� 
prmp; m   f i== �>> 6 S e l e c t   t h e   t a g s   t o   i n c l u d e :< ��?��
�� 
mlsl? m   l m��
�� boovtrue��  ��  ��  7 o      �� 0 thetags theTags5 @A@ l  x x�~�}�|�~  �}  �|  A BCB l  x x�{DE�{  D I C If any tags are seleceted the user, add them to the string of tags   E �FF �   I f   a n y   t a g s   a r e   s e l e c e t e d   t h e   u s e r ,   a d d   t h e m   t o   t h e   s t r i n g   o f   t a g sC GHG Z   x �IJ�z�yI >  x }KLK o   x {�x�x 0 thetags theTagsL m   { |�w
�w boovfalsJ X   � �M�vNM r   � �OPO b   � �QRQ b   � �STS o   � ��u�u 0 thecustomtags theCustomTagsT m   � �UU �VV  ,R o   � ��t�t 0 thetag theTagP o      �s�s 0 thecustomtags theCustomTags�v 0 thetag theTagN o   � ��r�r 0 thetags theTags�z  �y  H WXW l  � ��q�p�o�q  �p  �o  X YZY l  � ��n[\�n  [ > 8 Set the Category of the email selected, to categoryName   \ �]] p   S e t   t h e   C a t e g o r y   o f   t h e   e m a i l   s e l e c t e d ,   t o   c a t e g o r y N a m eZ ^_^ r   � �`a` J   � �bb c�mc o   � ��l�l 0 
mycategory 
myCategory�m  a n      ded m   � ��k
�k 
cCtge o   � ��j�j 0 	mymessage 	myMessage_ fgf l  � ��i�h�g�i  �h  �g  g hih l  � ��fjk�f  j ' ! Set the Sender Name of the email   k �ll B   S e t   t h e   S e n d e r   N a m e   o f   t h e   e m a i li mnm Q   � �opqo r   � �rsr n   � �tut 1   � ��e
�e 
pnamu o   � ��d�d 0 mysender mySenders o      �c�c 0 mysendername mySenderNamep R      �b�a�`
�b .ascrerr ****      � ****�a  �`  q k   � �vv wxw l  � ��_�^�]�_  �^  �]  x y�\y r   � �z{z n   � �|}| 1   � ��[
�[ 
radd} o   � ��Z�Z 0 mysender mySender{ o      �Y�Y 0 mysendername mySenderName�\  n ~~ l  � ��X�W�V�X  �W  �V   ��� r   � ���� m   � ��� ���  � o      �U�U 0 
first_name  � ��� r   � ���� m   � ��� ���  � o      �T�T 0 
first_name  � ��� l  � ��S�R�Q�S  �R  �Q  � ��� r   � ���� m   � ��� ���  ,� n     ��� 1   � ��P
�P 
txdl� 1   � ��O
�O 
ascr� ��� Q   ����N� s   ���� n   � ���� 2   � ��M
�M 
citm� o   � ��L�L 0 mysendername mySenderName� J      �� ��� o      �K�K 0 	last_name  � ��J� o      �I�I 0 
first_name  �J  � R      �H�G�F
�H .ascrerr ****      � ****�G  �F  �N  � ��� r  !��� m  �� ���  � n     ��� 1   �E
�E 
txdl� 1  �D
�D 
ascr� ��� l ""�C�B�A�C  �B  �A  � ��� r  "1��� b  "-��� b  ")��� o  "%�@�@ 0 
first_name  � m  %(�� ���   � o  ),�?�? 0 	last_name  � o      �>�> 0 myname myName� ��� l 22�=�<�;�=  �<  �;  � ��� l 22�:���:  � 8 2 Set myContent to ((content of myMessage) as text)   � ��� d   S e t   m y C o n t e n t   t o   ( ( c o n t e n t   o f   m y M e s s a g e )   a s   t e x t )� ��� r  2?��� c  2;��� n  27��� 1  37�9
�9 
ID  � o  23�8�8 0 	mymessage 	myMessage� m  7:�7
�7 
TEXT� o      �6�6 0 myid myID� ��� l @@�5�4�3�5  �4  �3  � ��� l @@�2���2  � $  Format the Outlook Linker URL   � ��� <   F o r m a t   t h e   O u t l o o k   L i n k e r   U R L� ��� r  @K��� b  @G��� m  @C�� ��� 0 o u t l i n k : / / o u t l o o k l i n k e r ?� o  CF�1�1 0 myid myID� o      �0�0 0 mycustomlink myCustomLink� ��� l LL�/�.�-�/  �.  �-  � ��� l LL�,���,  � . ( Format the task note (adding linefeeds)   � ��� P   F o r m a t   t h e   t a s k   n o t e   ( a d d i n g   l i n e f e e d s )� ��� r  L[��� b  LW��� b  LS��� o  LO�+�+ 0 mycustomlink myCustomLink� o  OR�*
�* 
ret � 1  SV�)
�) 
lnfd� o      �(�( 0 mynote myNote� ��� l \\�'�&�%�'  �&  �%  � ��� l \\�$���$  � * $ Replace &'s that are in the subject   � ��� H   R e p l a c e   & ' s   t h a t   a r e   i n   t h e   s u b j e c t� ��� r  \j��� n \h��� I  ]h�#��"�# 0 replace_chars  � ��� o  ]^�!�! 0 	mysubject 	mySubject� ��� m  ^a�� ���  &� �� � m  ad�� ���  a n d�   �"  �  f  \]� l     ���� o      �� 0 	mysubject 	mySubject�  �  � ��� l kk����  �  �  � ��� l kk�� �  � 4 . Create the tasks in 2Do (using the app's URL)     � \   C r e a t e   t h e   t a s k s   i n   2 D o   ( u s i n g   t h e   a p p ' s   U R L )� � I k���
� .sysoexecTEXT���     TEXT b  k� b  k� b  k�	 b  k�

 b  k� b  k� b  k� b  k| b  kx b  kt b  kr m  kn � & o p e n   ' t w o d o : / / / a d d ? m  nq � 
 t a s k = o  rs�� 0 	mysubject 	mySubject m  tw �  & f o r l i s t = m  x{   �!!  & n o t e = o  |�� 0 mynote myNote m  ��"" �##  & t a g s = o  ���� 0 
defaulttag 
defaultTag m  ��$$ �%%  ,	 o  ���� 0 myname myName o  ���� 0 thecustomtags theCustomTags m  ��&& �''  '�  �  �� 0 	mymessage 	myMessage o   * +�� $0 selectedmessages selectedMessages��   � m     ((                                                                                  OPIM  alis    �  Macintosh HD               ���H+   �Microsoft Outlook.app                                           R�Ț�V        ����  	                Microsoft Office 2011     ���      Ț�F     � ��  GMacintosh HD:Applications: Microsoft Office 2011: Microsoft Outlook.app   ,  M i c r o s o f t   O u t l o o k . a p p    M a c i n t o s h   H D  8Applications/Microsoft Office 2011/Microsoft Outlook.app  / ��  ��  ��   � )*) l     ����  �  �  * +,+ l     �-.�  - ; 5-----------------------------------------------------   . �// j - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -, 010 l     �23�  2 ; 5             FUNCTION: Replace Chars               --   3 �44 j                           F U N C T I O N :   R e p l a c e   C h a r s                               - -1 565 l     �
78�
  7 ; 5-----------------------------------------------------   8 �99 j - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -6 :;: i    <=< I      �	>��	 0 replace_chars  > ?@? o      �� 0 	this_text  @ ABA o      �� 0 search_string  B C�C o      �� 0 replacement_string  �  �  = k      DD EFE r     GHG l    I��I o     �� 0 search_string  �  �  H n     JKJ 1    � 
�  
txdlK 1    ��
�� 
ascrF LML r    NON n    	PQP 2    	��
�� 
citmQ o    ���� 0 	this_text  O l     R����R o      ���� 0 	item_list  ��  ��  M STS r    UVU l   W����W o    ���� 0 replacement_string  ��  ��  V n     XYX 1    ��
�� 
txdlY 1    ��
�� 
ascrT Z[Z r    \]\ c    ^_^ l   `����` o    ���� 0 	item_list  ��  ��  _ m    ��
�� 
TEXT] o      ���� 0 	this_text  [ aba r    cdc m    ee �ff  d n     ghg 1    ��
�� 
txdlh 1    ��
�� 
ascrb i��i L     jj o    ���� 0 	this_text  ��  ; k��k l     ��������  ��  ��  ��       ��l q��m �no��pqrst+��u�vwxyz��������  l �������������������������������������������������� 0 
defaulttag 
defaultTag�� 0 usertags userTags�� 0 taglist tagList�� 0 categoryname categoryName�� 0 replace_chars  
�� .aevtoappnull  �   � ****�� 0 msgcount msgCount�� $0 selectedmessages selectedMessages�� 0 	mysubject 	mySubject�� 0 mysender mySender�� 0 myheader myHeader�� 0 
mycategory 
myCategory�� 0 thecustomtags theCustomTags�� 0 thetags theTags�� 0 mysendername mySenderName�� 0 
first_name  �� 0 	last_name  �� 0 myname myName�� 0 myid myID�� 0 mycustomlink myCustomLink�� 0 mynote myNote��  ��  ��  �� m ��{�� {   � � � � �n ��=����|}���� 0 replace_chars  �� ��~�� ~  �������� 0 	this_text  �� 0 search_string  �� 0 replacement_string  ��  | ���������� 0 	this_text  �� 0 search_string  �� 0 replacement_string  �� 0 	item_list  } ��������e
�� 
ascr
�� 
txdl
�� 
citm
�� 
TEXT�� !���,FO��-E�O���,FO��&E�O���,FO�o ����������
�� .aevtoappnull  �   � **** k    ���  �����  ��  ��  � ������ 0 	mymessage 	myMessage�� 0 thetag theTag� =(������ �������������������������+����=��������U���������������������������������������������� "$&��
�� 
CMgs
�� .corecnte****       ****�� 0 msgcount msgCount
�� .sysodlogaskr        TEXT�� $0 selectedmessages selectedMessages
�� 
kocl
�� 
cobj
�� 
subj�� 0 	mysubject 	mySubject
�� 
sndr�� 0 mysender mySender
�� 
pMHd�� 0 myheader myHeader
�� 
cCtg�� 0 
mycategory 
myCategory�� 0 thecustomtags theCustomTags
�� 
prmp
�� 
mlsl�� 
�� .gtqpchltns    @   @ ns  �� 0 thetags theTags
�� 
pnam�� 0 mysendername mySenderName��  ��  
�� 
radd�� 0 
first_name  
�� 
ascr
�� 
txdl
�� 
citm�� 0 	last_name  �� 0 myname myName
�� 
ID  
�� 
TEXT�� 0 myid myID�� 0 mycustomlink myCustomLink
�� 
ret 
�� 
lnfd�� 0 mynote myNote�� 0 replace_chars  
�� .sysoexecTEXT���     TEXT�����*�,j E�O�k �j OhY hO*�,E�O{�[��l kh  ��,E�O��,E�O��,E�O*�b  /E` Oa E` Ob  a a a ea  E` O_ f ) #_ [��l kh _ a %�%E` [OY��Y hO_ kv��,FO �a ,E` W X  �a ,E` Oa E`  Oa !E`  Oa "_ #a $,FO !_ a %-E[�k/EQ` &Z[�l/EQ`  ZW X  hOa '_ #a $,FO_  a (%_ &%E` )O�a *,a +&E` ,Oa -_ ,%E` .O_ ._ /%_ 0%E` 1O)�a 2a 3m+ 4E�Oa 5a 6%�%a 7%a 8%_ 1%a 9%b   %a :%_ )%_ %a ;%j <[OY��U�� p ����� �  �� �� (������
�� 
inm ��  �M
�� kfrmID  q ��� J F w d :   P l e a s e   v e r i f y   y o u r   e m a i l   a d d r e s sr �����
�� 
type
�� ****UnAd� ��u�
�� 
pnamu ���  J a m e s   G i b b a r d� ����
�� 
radd� ��� $ j g i b b a r d @ g m a i l . c o m�  s ���� R e c e i v e d :   f r o m   D N V W S M X 1 . m c a f e e . c o m   ( 1 0 . 4 8 . 4 8 . 2 4 2 )   b y   m a i l . n a . n a i . c o m    ( 1 0 . 4 8 . 4 8 . 7 2 )   w i t h   M i c r o s o f t   S M T P   S e r v e r   i d   1 4 . 3 . 1 7 4 . 1 ;   T h u ,   6   F e b   2 0 1 4    0 6 : 5 7 : 5 0   - 0 5 0 0  R e c e i v e d - S P F :   p a s s   ( m a i l - q c 0 - f 1 8 1 . g o o g l e . c o m :   d o m a i n   o f   j g i b b a r d @ g m a i l . c o m   d e s i g n a t e s   2 0 9 . 8 5 . 2 1 6 . 1 8 1   a s   p e r m i t t e d   s e n d e r )   r e c e i v e r = m a i l - q c 0 - f 1 8 1 . g o o g l e . c o m ;   c l i e n t _ i p = 2 0 9 . 8 5 . 2 1 6 . 1 8 1 ;   e n v e l o p e - f r o m = j g i b b a r d @ g m a i l . c o m ;  R e c e i v e d :   f r o m   m a i l - q c 0 - f 1 8 1 . g o o g l e . c o m   ( u n k n o w n   [ 2 0 9 . 8 5 . 2 1 6 . 1 8 1 ] )   b y    D N V W S M X 1 . m c a f e e . c o m   w i t h   s m t p 	 ( T L S :   T L S v 1 / S S L v 3 , 1 2 8 b i t s , R C 4 - S H A ) 	   i d    0 a 5 e _ 1 9 0 5 _ d a e c e c c 0 _ f 4 6 6 _ 4 c 3 0 _ b 0 8 7 _ f f 9 b 7 f 4 7 f 9 6 6 ; 	 T h u ,   0 6   F e b   2 0 1 4   0 5 : 5 7 : 4 9    - 0 6 0 0  R e c e i v e d :   b y   m a i l - q c 0 - f 1 8 1 . g o o g l e . c o m   w i t h   S M T P   i d   e 9 s o 2 8 4 0 9 9 8 q c y . 2 6                  f o r   < j a m e s _ g i b b a r d @ m c a f e e . c o m > ;   T h u ,   0 6   F e b   2 0 1 4   0 3 : 5 7 : 4 9   - 0 8 0 0   ( P S T )  D K I M - S i g n a t u r e :   v = 1 ;   a = r s a - s h a 2 5 6 ;   c = r e l a x e d / r e l a x e d ;                  d = g m a i l . c o m ;   s = 2 0 1 2 0 1 1 3 ;                  h = m i m e - v e r s i o n : i n - r e p l y - t o : r e f e r e n c e s : d a t e : m e s s a g e - i d : s u b j e c t : f r o m : t o                    : c o n t e n t - t y p e ;                  b h = X + z H s P + k q K 2 G B H P Q f c 9 I 6 A s + j I + t Y 6 Z s L U 8 c r g W g a d o = ;                  b = d a Z L 7 G V R H l c 2 v a E Z Z M X u U n + F q t l 2 r E j H 6 S d l O c 1 f + E l 3 2 1 / 0 l Z l g b i M p W P p B N 7 F G T +                    P S l p I 6 1 k X K f / N U / q d O G G 4 / B C c z H R U A m G e 4 O N + 8 V e k Q M h H A y p x w p v q 5 y 0 H 6 m 4 7 U O A A r 7 c                    g V 6 W l V w l A W 2 N o P W 1 s G P z I 4 C m V 1 w c 5 2 O V m o 1 V d e R n k p n M a T K v p i y u V i u F X 3 K B / a z O f E B 7                    c J t S N K W M u k z g O Z F W p k s u q l 7 j V p + A 4 Q 0 8 a v U p y G 4 V j h m 4 e K q 9 t J w S 2 4 H 2 T I Z I u N e 9 2 t d i                    E 8 x i 5 5 Q C 3 e I 8 J i l 5 m M 2 6 G X u 5 Y X 0 P d b f X N J y o c O 5 Y U n E 4 8 c c 5 g m x u I r I J o 9 y 2 e O A U W A t D                    6 W n Q = =  M I M E - V e r s i o n :   1 . 0  X - R e c e i v e d :   b y   1 0 . 2 2 4 . 1 2 2 . 2 0   w i t h   S M T P   i d   j 2 0 m r 1 1 8 1 8 9 8 2 q a r . 8 2 . 1 3 9 1 6 8 7 8 6 9 2 2 0 ;    T h u ,   0 6   F e b   2 0 1 4   0 3 : 5 7 : 4 9   - 0 8 0 0   ( P S T )  R e c e i v e d :   b y   1 0 . 2 2 9 . 1 2 6 . 1   w i t h   H T T P ;   T h u ,   6   F e b   2 0 1 4   0 3 : 5 7 : 4 9   - 0 8 0 0   ( P S T )  I n - R e p l y - T o :   < 5 2 f 3 6 2 f c 8 7 8 7 b _ 6 b 0 2 3 f e 7 8 d 8 c 9 e 9 8 1 6 4 7 2 5 e @ z e n d e s k . c o m >  R e f e r e n c e s :   < 5 2 f 3 6 2 f c 8 7 8 7 b _ 6 b 0 2 3 f e 7 8 d 8 c 9 e 9 8 1 6 4 7 2 5 e @ z e n d e s k . c o m >  D a t e :   T h u ,   6   F e b   2 0 1 4   1 1 : 5 7 : 4 9   + 0 0 0 0  M e s s a g e - I D :   < C A L W _ 1 r A G Q t 7 F d 1 1 R G y E q R z V E q F x s e _ c u t s M T W u v Z W H D 8 r T c V 0 w @ m a i l . g m a i l . c o m >  S u b j e c t :   F w d :   P l e a s e   v e r i f y   y o u r   e m a i l   a d d r e s s  F r o m :   J a m e s   G i b b a r d   < j g i b b a r d @ g m a i l . c o m >  T o :   " j a m e s _ g i b b a r d @ m c a f e e . c o m "   < j a m e s _ g i b b a r d @ m c a f e e . c o m >  X - N A I - S p a m - F l a g :   N O  X - N A I - S p a m - L e v e l :  X - N A I - S p a m - T h r e s h o l d :   5  X - N A I - S p a m - S c o r e :   0 . 9  X - N A I - S p a m - V e r s i o n :   2 . 3 . 0 . 9 3 7 8   :   c o r e   < 4 8 4 4 >   :   i n l i n e s   < 4 9 4 >   :   s t r e a m s   < 1 1 1 9 2 7 1 >   :   u r i   < 1 6 6 7 0 3 8 >  R e t u r n - P a t h :   j g i b b a r d @ g m a i l . c o m  X - M S - E x c h a n g e - O r g a n i z a t i o n - A u t h S o u r c e :   M I V E X A P P 2 N 2 . c o r p . n a i . o r g  X - M S - E x c h a n g e - O r g a n i z a t i o n - A u t h A s :   I n t e r n a l  X - M S - E x c h a n g e - O r g a n i z a t i o n - A u t h M e c h a n i s m :   1 0  C o n t e n t - t y p e :   m u l t i p a r t / a l t e r n a t i v e ;  	 b o u n d a r y = " B _ 3 4 7 4 5 7 0 8 4 3 _ 3 2 7 2 5 9 1 "t �� (�~�}�|
�~ 
cCtg�} 
�| kfrmID  
�� boovfalsv ���  J a m e s   G i b b a r dw ���    J a m e s   G i b b a r dx ���  1 6 3 9 1 7y ��� < o u t l i n k : / / o u t l o o k l i n k e r ? 1 6 3 9 1 7z ��� @ o u t l i n k : / / o u t l o o k l i n k e r ? 1 6 3 9 1 7  
��  ��  ��  ascr  ��ޭ