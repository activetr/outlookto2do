========================
Outlook To 2Do - 1.0.0
========================

Author: James Gibbard (http://jgibbard.me.uk)
Website: http://jgibbard.me.uk/bitbucket/evernoteto2do

OutlookTo2Do allows you to easily create tasks in 2Do from your messages in 
Outlook. You can choose from a list of custom tags that you can optionally set.

Installation:
--------------
The best way I found to install AppleScripts into Outlook is to use them as
an OS X Service and assign them to a shortcut key.

1. Download the OS X Service DMG installer (OutlookTo2Do - http://bit.ly/1fLVJsi)
2. Install the service on your computer.
3. Open 'System Preferences', go to Keyboard -> Shortcuts -> Services
4. Select the ‘OutlookTo2Do’ service; Assign a shortcut key to the service.


Usage:
-------
1. Open Outlook
2. Select a note in the list
3. Press your chosen shortcut key.
4. Watch your selected message appear in 2Do as a task - and smile :)

Extra:
-------
Use the Outlook Linker (http://bit.ly/1fLW0LA) app with OutlookTo2Do to allow 
you to open the original emails from the link added to your task. 